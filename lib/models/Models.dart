import 'package:flutter/material.dart';

class ModelProduct {
  int id;
  String name;
  String imageUrl;
  String description;
  double price;
  ModelProduct({
    this.description,
    this.id,
    this.imageUrl,
    this.name,
    this.price,
  });
}

class ModelFilter {
  int id;
  EnumFilterType type;
  IconData icon;
  String name;
  List<ModelDropdownItems> listSubTypes;
  ModelFilter({this.type, this.icon, this.id, this.name, this.listSubTypes});
}

class ModelDropdownItems {
  String id;
  String name;

  ModelDropdownItems({
    @required this.id,
    @required this.name,
  });
}

class ModelSelectedFilter {
  String id;
  String name;
  EnumFilterType filterType;
  String filterName;
  ModelSelectedFilter({this.filterType, this.id, this.name, this.filterName});
}

enum EnumFilterType { color, liftControl, lightControl, material, style }
