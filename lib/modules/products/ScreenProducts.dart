import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task/helper/HelperFunction.dart';
import 'package:flutter_task/models/Models.dart';
import 'package:flutter_task/widgets/WidgetCacheImage.dart';

class ScreenProducts extends StatefulWidget {
  @override
  _ScreenProductsState createState() => _ScreenProductsState();
}

class _ScreenProductsState extends State<ScreenProducts> {
  List<ModelProduct> listProducts = [];
  List<ModelFilter> listFilters = [];
  List<ModelSelectedFilter> listSelectedFilters = [];
  int getCurrentProductIndex = 0;
  @override
  void initState() {
    super.initState();
    assignDataToProducts();
    assignDataToFilterList();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).primaryColor,
      appBar: AppBar(
        elevation: 0,
        actions: [
          IconButton(icon: Icon(Icons.account_circle), onPressed: () {})
        ],
      ),
      drawer: SafeArea(
        child: Drawer(
            child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Container(
              padding: EdgeInsets.symmetric(vertical: 30, horizontal: 10),
              width: MediaQuery.of(context).size.width,
              color: Theme.of(context).primaryColor,
              child: const Text(
                'Flutter Task',
                style: TextStyle(color: Colors.white, fontSize: 18),
              ),
            ),
          ],
        )),
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Visibility(
              visible: listSelectedFilters.isNotEmpty,
              child: Padding(
                padding: const EdgeInsets.only(left: 15, bottom: 20),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    InkWell(
                      onTap: () {
                        setState(() {
                          listSelectedFilters.clear();
                        });
                      },
                      child: Container(
                        padding: EdgeInsets.all(5),
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(100)),
                        child: Icon(
                          Icons.replay,
                          size: 14,
                          color: Colors.red,
                        ),
                      ),
                    ),
                    Expanded(
                      child: Container(
                          height: 35,
                          padding: EdgeInsets.only(left: 10),
                          child: ListView.builder(
                              scrollDirection: Axis.horizontal,
                              itemCount: listSelectedFilters.length,
                              shrinkWrap: true,
                              itemBuilder: (context, index) {
                                var appliedFilterDetails =
                                    listSelectedFilters[index];
                                return Padding(
                                  padding: const EdgeInsets.only(right: 10),
                                  child: Chip(
                                    label: Text(
                                        '${appliedFilterDetails.filterName}: ${appliedFilterDetails.name}'),
                                    labelStyle: TextStyle(
                                        color: Theme.of(context).primaryColor),
                                    backgroundColor: Colors.white,
                                    deleteIconColor:
                                        Theme.of(context).primaryColor,
                                    labelPadding: EdgeInsets.only(left: 5),
                                    deleteIcon: Icon(
                                      Icons.close,
                                      size: 16,
                                    ),
                                    onDeleted: () {
                                      setState(() {
                                        listSelectedFilters.removeAt(index);
                                      });
                                    },
                                  ),
                                );
                              })),
                    )
                  ],
                ),
              ),
            ),
            widgetCarouselProducts(),
            SizedBox(
              height: 20,
            ),
            widgetDisplayFilters()
          ],
        ),
      ),
    );
  }

  Widget widgetCarouselProducts() {
    return CarouselSlider.builder(
        itemCount: listProducts.length,
        itemBuilder: (context, index, tempVar) {
          var productDetails = listProducts[index];
          return Card(
            shape:
                RoundedRectangleBorder(borderRadius: BorderRadius.circular(10)),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Stack(
                  alignment: Alignment.bottomLeft,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(10),
                          topRight: Radius.circular(10)),
                      child: WidgetCacheImage(
                        imageUrl: productDetails.imageUrl,
                        height: 250,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(horizontal: 10),
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: [
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  '${productDetails.name}',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 20),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 3, bottom: 10),
                                  child: Text(
                                    '1511 mm x 2057 mm',
                                    style: TextStyle(
                                      color: Colors.white,
                                    ),
                                  ),
                                )
                              ],
                            ),
                          ),
                          Flexible(
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Text(
                                  'starting from',
                                  style: TextStyle(
                                      color: Colors.white, fontSize: 16),
                                ),
                                Padding(
                                  padding:
                                      const EdgeInsets.only(top: 3, bottom: 10),
                                  child: Text(
                                    '${productDetails.price}\$',
                                    style: TextStyle(
                                        color: Colors.white,
                                        fontSize: 18,
                                        fontWeight: FontWeight.w700),
                                  ),
                                )
                              ],
                            ),
                          )
                        ],
                      ),
                    )
                  ],
                ),
                Padding(
                  padding: const EdgeInsets.all(15.0),
                  child: Text(
                    '${productDetails.description}',
                    overflow: TextOverflow.ellipsis,
                    maxLines: 2,
                    style: TextStyle(fontWeight: FontWeight.w500),
                  ),
                ),
                Visibility(
                  visible: getCurrentProductIndex == index,
                  child: Container(
                    margin: EdgeInsets.symmetric(horizontal: 15),
                    width: MediaQuery.of(context).size.width,
                    child: RaisedButton(
                      onPressed: () {},
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50)),
                      color: Colors.green[400],
                      child: Text(
                        'Customize',
                        style: TextStyle(fontSize: 16),
                      ),
                      textColor: Colors.white,
                    ),
                  ),
                )
              ],
            ),
          );
        },
        options: CarouselOptions(
          height: 400,
          viewportFraction: 0.8,
          initialPage: 0,
          enableInfiniteScroll: false,
          reverse: false,
          autoPlay: false,
          enlargeCenterPage: true,
          onPageChanged: (index, reason) {
            setState(() {
              getCurrentProductIndex = index;
            });
          },
          scrollDirection: Axis.horizontal,
        ));
  }

  widgetDisplayFilters() {
    return Container(
      height: 75,
      padding: EdgeInsets.only(left: 5),
      child: ListView.builder(
          scrollDirection: Axis.horizontal,
          itemCount: listFilters.length,
          shrinkWrap: true,
          itemBuilder: (context, index) {
            var filterDetails = listFilters[index];
            return Container(
              margin: EdgeInsets.only(right: 15),
              child: InkWell(
                onTap: () {
                  //show dialog
                  HelperFunction.showSelectorDialog(context,
                      listStatic: filterDetails.listSubTypes ?? [],
                      title: 'Select filter', callBack: (value) {
                    setState(() {
                      listSelectedFilters.add(ModelSelectedFilter(
                          id: value.id,
                          name: value.name,
                          filterType: filterDetails.type,
                          filterName: filterDetails.name));
                    });
                  },
                      //passing selected filters so that user can multiple select that particular filter
                      listOfSelectedData: listSelectedFilters
                          .where((element) =>
                              element.filterType == filterDetails.type)
                          .map<String>((e) => e.id)
                          .toList());
                },
                child: Column(
                  children: [
                    Container(
                      padding: EdgeInsets.all(10),
                      decoration: BoxDecoration(
                          color: Colors.white,
                          borderRadius: BorderRadius.circular(100)),
                      child: Icon(
                        filterDetails.icon,
                        color: Theme.of(context).primaryColor,
                        size: 26,
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(top: 5),
                      child: Text(
                        '${filterDetails.name}',
                        style: TextStyle(color: Colors.white, fontSize: 12),
                      ),
                    )
                  ],
                ),
              ),
            );
          }),
    );
  }

  assignDataToFilterList() {
    listFilters.addAll([
      ModelFilter(
          id: 0,
          type: EnumFilterType.color,
          name: 'Color',
          icon: Icons.palette,
          listSubTypes: [
            ModelDropdownItems(id: '0', name: 'Red'),
            ModelDropdownItems(id: '1', name: 'White'),
          ]),
      ModelFilter(
          id: 1,
          type: EnumFilterType.liftControl,
          name: 'Lift Control',
          listSubTypes: [
            ModelDropdownItems(id: '0', name: 'Motorized'),
            ModelDropdownItems(id: '1', name: 'Un-montorized'),
          ],
          icon: Icons.precision_manufacturing_outlined),
      ModelFilter(
          id: 2,
          type: EnumFilterType.lightControl,
          name: 'Light Control',
          icon: Icons.brightness_6),
      ModelFilter(
          id: 3,
          type: EnumFilterType.material,
          name: 'Material',
          icon: Icons.ac_unit),
      ModelFilter(
          id: 4,
          type: EnumFilterType.style,
          name: 'Style',
          icon: Icons.ac_unit),
    ]);
  }

  assignDataToProducts() {
    listProducts.addAll([
      ModelProduct(
          id: 0,
          name: 'Cellular Shades',
          description:
              'Cellular shades with many possibilties of issues and heating effects',
          imageUrl:
              'https://d2e5ushqwiltxm.cloudfront.net/wp-content/uploads/sites/12/2016/02/09120423/Pullman-Executive-Room-King-Bed-1.jpg',
          price: 120.00),
      ModelProduct(
          id: 1,
          name: 'Solar Shades',
          description:
              'Solar shades with many possibilties of issues and heating effects',
          imageUrl:
              'https://d2e5ushqwiltxm.cloudfront.net/wp-content/uploads/sites/12/2016/02/09120423/Pullman-Executive-Room-King-Bed-1.jpg',
          price: 210.00),
      ModelProduct(
          id: 2,
          name: 'GreenHouse Shades',
          description:
              'Greenhouse shades with many possibilties of issues and heating effects',
          imageUrl:
              'https://d2e5ushqwiltxm.cloudfront.net/wp-content/uploads/sites/12/2016/02/09120423/Pullman-Executive-Room-King-Bed-1.jpg',
          price: 20.00),
    ]);
  }
}
