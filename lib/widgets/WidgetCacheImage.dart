import 'dart:io';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class WidgetCacheImage extends StatelessWidget {
  String imageUrl;
  double height, width;
  Widget errorWidget;
  WidgetCacheImage({this.imageUrl, this.height, this.width, this.errorWidget});
  @override
  Widget build(BuildContext context) {
    return CachedNetworkImage(
      width: width == null ? MediaQuery.of(context).size.width : width,
      fit: BoxFit.cover,
      height: height,
      placeholder: (context, url) => Center(
        child: Platform.isIOS
            ? CupertinoActivityIndicator()
            : Container(
                height: 20,
                width: 20,
                child: CircularProgressIndicator(
                  strokeWidth: 2,
                ),
              ),
      ),
      errorWidget: (context, url, error) =>
          errorWidget == null ? Icon(Icons.warning) : errorWidget,
      imageUrl: imageUrl,
    );
  }
}
