import 'package:flutter/material.dart';
import 'package:flutter_task/helper/HelperFunction.dart';
import 'package:flutter_task/models/Models.dart';

class Dropdown extends StatefulWidget {
  String title;
  List<ModelDropdownItems> items;
  List<String> selectedItems;

  Function(ModelDropdownItems) onsingleSelect;

  Dropdown({
    @required this.title,
    @required this.items,
    @required this.selectedItems,
    this.onsingleSelect,
  });
  @override
  _DropdownState createState() => _DropdownState();
}

class _DropdownState extends State<Dropdown> {
  List<ModelDropdownItems> selectedItems = [];
  @override
  void initState() {
    super.initState();

    if (widget.selectedItems.isNotEmpty) {
      widget.selectedItems.forEach((id) {
        selectedItems.add(widget.items
            .firstWhere((element) => element.id == id, orElse: null));
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(8.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(left: 10),
            child: Text(
              widget.title ?? "Select Item",
              style: TextStyle(fontSize: 16, color: Colors.black),
            ),
          ),
          (widget.items).isEmpty
              ? Center(
                  child: Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: Text(
                      "Items not Available !",
                      style: TextStyle(
                          fontSize: 16,
                          color: Colors.black,
                          fontWeight: FontWeight.bold),
                    ),
                  ),
                )
              : Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: ListView.builder(
                    shrinkWrap: true,
                    itemCount: widget.items.length,
                    physics: NeverScrollableScrollPhysics(),
                    itemBuilder: (context, index) {
                      var item = widget.items[index];

                      return InkWell(
                        onTap: () {
                          if (widget.onsingleSelect != null) {
                            if (widget.selectedItems.contains(item.id)) {
                              HelperFunction.showFlushbarError(
                                  context, "Filter already selected");
                            } else {
                              widget.onsingleSelect(item);
                            }
                          }
                        },
                        child: Container(
                          color: widget.selectedItems.contains(item.id)
                              ? Colors.grey.shade300
                              : null,
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            children: [
                              Container(
                                  margin: EdgeInsets.only(
                                      bottom: 10, top: 10, left: 5),
                                  child: Row(
                                    children: [
                                      Expanded(
                                        flex: 1,
                                        child: Text(
                                          item.name,
                                          style: TextStyle(
                                              fontSize: 18,
                                              color: Colors.black),
                                        ),
                                      ),
                                      if (widget.selectedItems
                                          .contains(item.id))
                                        Container(
                                            margin: EdgeInsets.only(right: 5),
                                            child: Icon(
                                              Icons.stop,
                                              color: Colors.grey,
                                            ))
                                    ],
                                  )),
                              Divider(
                                height: 1,
                                thickness: 1,
                              )
                            ],
                          ),
                        ),
                      );
                    },
                  ),
                ),
        ],
      ),
    );
  }
}
