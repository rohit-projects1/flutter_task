import 'package:flutter/material.dart';
import 'package:flutter_task/models/Models.dart';
import 'package:flutter_task/widgets/WidgetDropDown.dart';

class SelectorFilter extends StatefulWidget {
  final Function(ModelDropdownItems) callBackSelected;
  final List<String> listOfSelectedData;
  final String title;
  final List<ModelDropdownItems> listStatic;
  SelectorFilter(
      {this.listOfSelectedData,
      this.callBackSelected,
      this.listStatic,
      this.title});
  @override
  _SelectorFilterState createState() => _SelectorFilterState();
}

class _SelectorFilterState extends State<SelectorFilter> {
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      contentPadding: EdgeInsets.zero,
      content: Container(
        width: double.maxFinite,
        child: SingleChildScrollView(
          child: Dropdown(
            title: widget.title != null ? widget.title : "Title",
            items: widget.listStatic,
            selectedItems: widget.listOfSelectedData ?? [],
            onsingleSelect: (singleSelectData) {
              widget.listOfSelectedData.add(singleSelectData.id);
              widget.callBackSelected(singleSelectData);
              Navigator.of(context).pop();
            },
          ),
        ),
      ),
      actions: [
        Padding(
          padding: const EdgeInsets.all(8.0),
          child: InkWell(
            onTap: () {
              Navigator.of(context).pop();
            },
            child: Text(
              'Close',
              style: TextStyle(color: Colors.red),
            ),
          ),
        )
      ],
    );
  }
}
