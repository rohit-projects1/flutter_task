import 'package:another_flushbar/flushbar.dart';
import 'package:flutter/material.dart';
import 'package:flutter_task/models/Models.dart';
import 'package:flutter_task/selectors/SelectorFilter.dart';

class HelperFunction {
  static showFlushbarError(BuildContext context, String msg) {
    Flushbar(
      margin: EdgeInsets.all(8),
      borderRadius: 8,
      backgroundColor: Colors.red,
      flushbarPosition: FlushbarPosition.TOP,
      message: msg,
      duration: Duration(seconds: 4),
    )..show(context);
  }

  static showSelectorDialog(context,
      {listOfSelectedData,
      title,
      List<ModelDropdownItems> listStatic,
      Function(ModelDropdownItems) callBack}) async {
    await showDialog(
        context: (context),
        builder: (context) {
          return SelectorFilter(
            title: title,
            listStatic: listStatic,
            listOfSelectedData: listOfSelectedData,
            callBackSelected: (selectedItem) {
              callBack(selectedItem);
            },
          );
        });
  }
}
